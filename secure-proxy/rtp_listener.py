from twisted.internet.protocol import DatagramProtocol
from header_parser import parse_headers
import socket
from ssl import SSLSocket
import logging
import os
import base64
import Crypto.Cipher.AES as AES
import hashlib

logger = logging.getLogger('RTP')


class RtpListener(DatagramProtocol):
    """The RtpListener is responsible for listening for RTP packets and
    encrypting them using AES encryption for safe transit (if outgoing) and for
    decrypting packets (if incoming). This protects the media stream of the
    conversation
    """

    def __init__(self, incoming, hostname_lookup, current_call):
        self.incoming = incoming
        self.hostname_lookup = hostname_lookup
        self.current_call = current_call
        self.current_call_id = None
        self.cipher = None

    def datagramReceived(self, data, (host, port)):
        """Handles the receipt of an RTP datagram"""
        
        if not self.incoming:
            self.processOutgoing(data)
        else:
            self.processIncoming(data)

    def initCipher(self, call_id, key):
        """Initializes the cipher for a new call with the respective key"""
        if self.current_call_id == call_id:
            # If we are still on the same call, skip...
            return
        
        # Initialize the AES cipher and set the current call ID
        self.cipher = AES.new(
            key, AES.MODE_CTR, counter=lambda: '1' * AES.block_size)
        self.current_call_id = self.current_call['id']

    def processOutgoing(self, data):
        """Encrypt and outgoing packet and send out on the UDP socket"""
        if 'id' not in self.current_call or self.current_call['id'] is None:
            logger.warn('Received outgoing RTP data, but no current call.')
            return

        if 'key' not in self.current_call:
            logger.warn('Received outgoing RTP data, but no key yet.')
            return

        call_id = self.current_call['id']
        key = self.current_call['key']
        self.initCipher(call_id, key)

        host = self.hostname_lookup[self.current_call['id']]
        logger.debug('Received outgoing data... sending to %s', host)
        logger.debug('Outgoing Fingerprint: %s', hashlib.md5(data).hexdigest())
        self.transport.write(self.cipher.encrypt(data), (host, 62568))

    def processIncoming(self, data):
        """Decrypt an incoming packet and pass it to its respective local
        RTP socket
        """
        if 'in_audio_host' not in self.current_call or 'in_audio_port' not in self.current_call:
            logger.warn('Received incoming data, but no current call.')

        if 'key' not in self.current_call:
            logger.warn('Received incoming data, but no key yet.')
            return

        call_id = self.current_call['id']
        key = self.current_call['key']
        self.initCipher(call_id, key)
        
        # The host and the port are cached from the original session init
        host = self.current_call['in_audio_host']
        port = int(self.current_call['in_audio_port'])

        data = self.cipher.decrypt(data)
        logger.debug('Received incoming data... forwarding to %i', port)
        logger.debug('Incoming Fingerprint: %s', hashlib.md5(data).hexdigest())
        self.transport.write(data, (host, port))
