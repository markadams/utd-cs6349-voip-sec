from twisted.internet.protocol import Protocol, Factory
from header_parser import parse_headers
import socket
import logging
import uuid
import string
import base64

logger = logging.getLogger('SSL')


class SslReceiver(Protocol):
    """ Responsible for receiving SSL socket connections from remote hosts"""
    add_server_header = ['SIP/2.0 200 OK']

    def dataReceived(self, data):
        """ Process packets received over SSL from a remote host """
        data = data.split('\r\n')
        first_line = data[0:1]
        other_lines = data[1:]

        headers, info, body = parse_headers(data)
        recp = info['from']

        sender = self.transport.socket.getpeername()[0]

        if 'Call-ID' in headers:
            self.factory.remote_addr_lookup[headers['Call-ID'][0]] = sender

        if recp not in self.factory.remote_addr_lookup:
            self.factory.remote_addr_lookup[recp] = sender

        logger.info('%s >>> %s' % (sender, first_line[0]))

        # Transform the packet before passing it on (if necessary)
        tosend = self.processData(first_line, headers, body)
        self.factory.udp_sock.write(tosend, ('127.0.0.1', 5060))

    def processData(self, first_line, headers, body):
        """Transforms a packet for secure communication"""
        data = []

        cseq = headers['CSeq'][0]
        remote = None

        if (first_line[0].startswith('SIP/2.0 200 OK') and 'REGISTER' not in cseq) or first_line[0].startswith('INVITE'):
            # If it looks like we've started a new call, store the call ID
            call_id = headers['Call-ID'][0]
            self.factory.current_call['id'] = call_id

        if 'X-VoipSec-Key' in headers:
            # If an AES key header is present, store the key for RTP
            value = headers['X-VoipSec-Key'][0]
            self.factory.current_call['key'] = base64.b64decode(value)
            logger.info('AES key detected: %s', value)
            del headers['X-VoipSec-Key']

        if 'X-VoipSec-Remote' in headers and not first_line[0].startswith('BYE') and 'Contact' in headers:
            # If a X-VoipSec-Remote header is present, store the remote address
            # for that call ID for later use when we need to send a direct msg
            # like an ACK
            remote = headers['X-VoipSec-Remote'][0]
            call_id = headers['Call-ID'][0]
            self.factory.remote_addr_lookup[call_id] = remote
            del headers['X-VoipSec-Remote']

            # Add a contact header instructing the local SIP client to use
            # the local UdpProxy to contact the remote host
            contact = headers['Contact'][0].split('@')
            contact[1] = '127.0.0.1:15060>'
            del headers['Contact']
            headers['Contact'] = ['@'.join(contact)]

        elif (first_line[0] == 'SIP/2.0 200 OK' or first_line[0].startswith('INVITE')) and 'REGISTER' not in cseq:
            # If a Remote header is not present, add one
            headers[
                'X-VoipSec-Remote'] = [self.transport.socket.getpeername()[0]]

        final_body = []

        contains_sdp = first_line[0].startswith(
            'INVITE') or first_line[0].startswith('SIP/2.0 200 OK')

        for line in body:
            if contains_sdp and line.startswith('c=IN IP4'):
                # Parse through the body and find the IP host section.
                # Replace the value with localhost to redirect RTP
                parts = line.split(' ')
                self.factory.current_call['in_audio_host'] = parts[2]
                final_body.append('c=IN IP4 127.0.0.1')
                continue

            if contains_sdp and line.startswith('m=audio'):
                # Parse through and also find the Audio port and redirect
                # to the RTP lsitener
                parts = line.split(' ')
                self.factory.current_call['in_audio_port'] = parts[1]
                final_body.append('m=audio 62569 ' + ' '.join(parts[2:]))
                continue

            final_body.append(line)
        
        # Recalculate the content length since it may have been edited
        headers['Content-Length'] = [str(len('\r\n'.join(final_body)))]
        
        # Reassemble the headers and body back into a string and return
        for k, v in headers.items():
            for iv in v:
                data.append(k + ': ' + iv)

        return '\r\n'.join(first_line + data + [''] + final_body)


class SslReceiverFactory(Factory):
    protocol = SslReceiver

    def __init__(self, forward_to_sock, hostname_lookup, current_call):
        self.udp_sock = forward_to_sock
        self.remote_addr_lookup = hostname_lookup
        self.current_call = current_call
