from collections import OrderedDict

import re

re_call_id = re.compile('Call-ID: (.+)')
re_to = re.compile('To: <sip:(.+@.+)>')
re_from = re.compile('From: <sip:(.+@.+)>')


def index_starts_with(needle, haystack):
    """Determines whether or not a line starting with needle is in the haystack
    """
    for idx, item in enumerate(haystack):
        if item.startswith(needle):
            return idx

    return -1


def parse_headers(data):
    """ Parses out separates out the headers and the body content from a SIP
    message
    """
    parsed = {}
    info = OrderedDict()
    
    # Find where the content length header is
    content_len_hdr = index_starts_with('Content-Length', data)

    body = ''
    
    # Separate out the headers from the content body
    if content_len_hdr:
        body = data[content_len_hdr + 2:]
        data = data[:content_len_hdr + 1]

    # Parse out the type of the packet
    parsed['type'] = data[0].split(' ')[0]

    # Create an ordered dict to store headers
    headers = OrderedDict()
    
    # Iterate through the headers and store them in the dict
    for k, v in (tuple(d.strip().split(':', 1)) for d in data[1:] if d.strip() != ''):
        if k not in headers:
            headers[k] = []

        headers[k].append(v.strip())
    
    # Apply regular expressions to parse out certain parts of the message
    for line in data[1:]:
        match = re_call_id.match(line)

        if match != None:
            parsed['call_id'] = match.group(1)

        match = re_to.match(line)

        if match != None:
            parsed['to'] = match.group(1)
            parsed['to_host'] = parsed['to'].split('@')[1]

        match = re_from.match(line)

        if match != None:
            parsed['from'] = match.group(1)
    
    # Return the dict of headers, the parsed data, and the remaining body.
    return headers, parsed, body
