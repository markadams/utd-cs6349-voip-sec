
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

from twisted.internet.protocol import DatagramProtocol, Factory
from twisted.internet import reactor, ssl

import argparse
import logging

from udp_proxy import UdpProxy
from ssl_receiver import SslReceiverFactory
from rtp_listener import RtpListener


def parse_args():
    parser = argparse.ArgumentParser(description='A secure SIP proxy')
    parser.add_argument(
        '--sip-port', type=int, help='The UDP port to listen on', default=15060)
    parser.add_argument(
        '--ssl-port', type=int, help='The SSL port to listen on for encrypted SSL', default=10433)
    parser.add_argument(
        '--debug', action='store_true', help='Enable debug log messages')
    parser.add_argument(
        '--rtp-port', type=int, help='The UDP port to listen on for RTP.', default=62568)

    args = parser.parse_args()

    return args


def config_logging(is_debug):
    formatter = logging.Formatter(
        '%(asctime)s - %(levelname)s[%(name)s] - %(message)s', '%H:%M:%S')
    level = logging.DEBUG if is_debug else logging.INFO

    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    ch.setLevel(level)

    logger = logging.getLogger()
    logger.setLevel(level)
    logger.addHandler(ch)

if __name__ == '__main__':
    args = parse_args()
    config_logging(args.debug)

    hostname_lookup = {}
    current_call = {}

    # Setup UDP Proxy
    logging.info('UDP proxy listening on UDP port %d', args.sip_port)
    udp = UdpProxy(hostname_lookup, current_call)
    reactor.listenUDP(args.sip_port, udp)

    # Setup SSL Receiver
    factory = SslReceiverFactory(udp.transport, hostname_lookup, current_call)

    logging.info('SSL Listener listening on TCP port %d', args.ssl_port)
    reactor.listenSSL(args.ssl_port, factory, ssl.DefaultOpenSSLContextFactory(
        'keys/voipsec_client.key', 'keys/voipsec_client.crt'))

    # Setup two RTP listeners
    logging.info(
        'RTP listener listening for incoming on UDP port %d', args.rtp_port)
    rtp_in = RtpListener(True, hostname_lookup, current_call)
    reactor.listenUDP(args.rtp_port, rtp_in)

    logging.info(
        'RTP listener listening for outgoing on UDP port %d', args.rtp_port + 1)
    rtp_out = RtpListener(False, hostname_lookup, current_call)
    reactor.listenUDP(args.rtp_port + 1, rtp_out)

    # Start listening for traffic
    reactor.run()
